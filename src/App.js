import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { Home } from './containers';

export default function App() {
  return (
    <Route path="/" component={Home} />
  );
}

App.defaultProps = {
};

App.propTypes = {
  children: PropTypes.element.isRequired
};
