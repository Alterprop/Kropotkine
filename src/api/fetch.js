import 'whatwg-fetch';
import Promise from 'native-promise-only';

export const Fetch = ops => {
  const options = Object.assign(
    {},
    {
      method: 'GET',
      contentType: 'application/json',
      cache: 'default',
      mode: 'cors',
      redirect: 'follow',
      referrer: 'no-referrer'
    },
    ops
  );

  const headers = new Headers();

  headers.append('Content-Type', options.contentType);

  if (!navigator.cookieEnabled && localStorage.getItem('token')) {
    headers.append('Authorization', localStorage.getItem('token'));
  }

  if (options.headers) {
    Object.keys(options.headers).forEach(header =>
      headers.append(header, options.headers[header])
    );
  }

  return fetch(`${options.url}`, options)
    .then(response => response.json())
    .then(response => Object.assign({}, response, { ok: 1 }))
    .catch(error =>
      Object.assign({}, error, {
        ok: 0
      })
    );
};

export const Timeout = time =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve({ ok: 0, error: 'Timeout' });
    }, time || 30000);
  });

export default function(fetchOptions, timeoutOptions) {
  return Promise.race([Fetch(fetchOptions), Timeout(timeoutOptions)]);
}
