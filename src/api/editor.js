export const commandList = ['bold', 'italic', 'underline'];

export function enabledCommands() {
  return commandList.filter(command=> document.queryCommandEnabled(command));
}

export function getX(event) {
  const isMobile = document.documentElement.clientWidth < 960;
  if (event.nativeEvent.pageX - 150 < (isMobile ? 0 : 340)) {
    return isMobile ? 16 : 340;
  } else if (event.nativeEvent.pageX > document.documentElement.clientWidth - 300) {
    return document.documentElement.clientWidth - 300;
  } else {
    return event.nativeEvent.pageX - 150;
  }
}

export function returnFocus(selection) {
  if (selection && selection.isCollapsed) {
    const range = document.createRange();
    range.setStart(selection.focusNode, selection.focusOffset);
    selection.removeAllRanges();
    selection.addRange(range);
  }
}
