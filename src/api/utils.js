export function isValid(id) {
  const regx = new RegExp('^[0-9a-fA-F]{24}$');
  return regx.test(id);
}

export const objProp = (props, value) => {
  const newObj = {};
  if (typeof props === 'string') {
    newObj[props] = value;
  }  else {
    const propsArray = Array.isArray(props) ? props : Object.keys(props);
    propsArray.forEach(prop => (newObj[prop] = value));
  }
  return newObj;
};

export default { isValid };
