const opciones = () => ({
  quality: 100,
  targetHeight: 200,
  targetWidth: 200,
  destinationType: '', //Camera.DestinationType.FILE_URI, //Camera.DestinationType.DATA_URL,
  sourceType: '', //Camera.PictureSourceType.CAMERA,
  encodingType: '', //Camera.EncodingType.JPEG,
  mediaType: '', //Camera.MediaType.PICTURE,
  allowEdit: true,
  correctOrientation: true
});

export const Foto = opcs => {
  return new Promise((resolve, reject) => {
    navigator.camera.getPicture(
      base64 => {
        let img = document.createElement('img');
        img.src = base64;

        resolve({
          html: img,
          src: base64
        });
      },
      err => reject(err),
      Object.assign({}, opciones(), opcs)
    );
  });
};

export const Galeria = opcs => {
  return Foto(
    Object.assign(
      {},
      { sourceType: '' }, //Camera.PictureSourceType.SAVEDPHOTOALBUM },
      opcs
    )
  );
};

const proto = {
  foto: Foto,
  galeria: Galeria
};

export default Object.create(proto);
