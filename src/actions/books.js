import api from '../api/api';
import { find } from './notes';
const  BOOK_CREATE = 'BOOK_CREATE';
const  BOOK_CREATE_ERROR = 'BOOK_CREATE_ERROR';

export const newBook = bookKey => ({
  type: BOOK_CREATE,
  key:  bookKey
});

export const newBookError = (error) => ({
  type: BOOK_CREATE_ERROR,
  payload: new Error(error),
  error: true
});

export const createBook = (key, persistent) => {

  key = key || api.getKey();

  return dispatch => {
    return api
      .find(key, persistent)
      .then(res => dispatch(find(res)))
      .then(() => dispatch(newBook(key)))
      .catch(err => dispatch(newBook(err)));
  };
};

export default {
  createBook,
};
