import notes from './notes';
import books from './books';
import events from './events';

import { combineReducers } from 'redux';

export default combineReducers({
  notes,
  books,
  events,
});
