const initialState = {
  notes: [],
  current: {}
};

export default (state = initialState, payload) => {
  switch (payload.type) {
  case 'NOTES_FIND':
    return Object.assign({}, state, { notes: payload.notes });
  case 'NOTES_INSERT':
    return Object.assign({}, state, {
      notas: [].concat(state.notes, [payload.note])
    });
  case 'NOTES_FINDONE':
  case 'NOTES_REMOVE_CURRENT':
    return Object.assign({}, state, { current: payload.note });
  case 'NOTES_UPDATE':
    return Object.assign({}, state, { notes:
      state.notes.filter(note => note._id === payload.note._id ? payload.note : note )
    });
  case 'NOTES_REMOVE':
    return Object.assign({}, state,  { notes: state.notes.filter(note => note._id !== payload.id) });
  default:
    return state;
  }
};
