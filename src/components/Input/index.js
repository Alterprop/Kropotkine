import React from 'react';
import PropTypes from 'prop-types';

export default function Input(props) {
  const { type } = props;
  const isCheckbox = (type === 'checkbox');
  return (
    <div className={ isCheckbox  ? 'checkbox-container' : 'input-container'  }>
      <input
        {...props}
      />
      <span className={ isCheckbox  ? 'checkbox-box' : 'input-line'  } />
      {props.label ? <label htmlFor={props.name}>{props.label}</label> : null}
    </div>
  );
}

Input.defaultProps = {
  label: null,
  type: 'text'
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
};
