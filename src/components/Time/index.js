import React from 'react';
import PropTypes from 'prop-types';
import Floreal from '../../lib/floreal';
import { Dropdown } from '../Menu';

const capitalize = str => `${str.charAt(0).toUpperCase()}${str.slice(1)}`;

const formatDate = (date, burgeois) => {

  if (burgeois) {
    const userLang = navigator.language || navigator.userLanguage;
    const event = new Date(date);
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return event.toLocaleDateString(userLang, options);
  }

  const floreal = new Floreal(date || Date.now());
  return {
    fullDate: `${capitalize(floreal.dayName())} ${floreal.day()} ${capitalize(floreal.monthName('es'))}`,
    monthName: capitalize(floreal.monthName('es')),
    monthImage: floreal.monthFigure()
  };
};

export default function Time(props) {

  const { fullDate, monthName, monthImage } = formatDate(props.time);
  return (
    <span className="time">
      <Dropdown openMenu={true}>
        <figure className="time-thumb" style={{ backgroundImage: `url(${monthImage})` }} title={monthName} />
        <div className="time-content">
          <figure className="time-image" style={{ backgroundImage: `url(${monthImage})` }} title={monthName} />
          <time style={{ padding: '16px' }}>{ fullDate }</time>
          <time style={{ padding: '0 16px 16px' }}>{
            `${formatDate(props.time, true)}`
          }</time>
        </div>
      </Dropdown>
      <time>{ fullDate }</time>
    </span>
  );

}

Time.defaultProps = {

};

Time.propTypes = {
  time: PropTypes.instanceOf(Date)
};
