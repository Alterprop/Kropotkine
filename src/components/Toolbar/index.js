import React from 'react';
import PropTypes from 'prop-types';
import NavBar from '../NavBar';
import { DropdownMenu } from '../Menu';
import BackButton from '../BackButton';
import ButtonBar from '../ButtonBar';
import Time from '../Time';

export default function Toolbar(props) {
  const {
    hasBackButton,
    backFunc,
    backIcon,
    navItems,
    actionItems,
    backItems,
    title,
    time,
    actionClasses,
  } = props;
  return (
    <header role="header" className={ `toolbar ${actionClasses}` }>

      {hasBackButton ? <BackButton icon={backIcon} backFunc={backFunc} /> : null}
      {backItems ? (
        <ButtonBar items={backItems} />
      ) : null}

      {time ? <Time time={time} backButton /> : null}

      {navItems ? <NavBar paths={navItems} type="toolbar-nav" /> : null}

      {!title && !time ? <h1></h1> : null}

      {actionItems ? (
        <ButtonBar items={actionItems} />
      ) : null}

      {title ? <h1>{title}</h1> : null}

    </header>
  );
}

Toolbar.defaultProps = {
  title: null,
  right: null,
  left: null,
  navItems: null,
  actionItems: null,
  hasBackButton: false,
  time: null,
  backIcon: null,
  actionClasses: ''
};

Toolbar.propTypes = {
  title: PropTypes.string,
  right: PropTypes.element,
  left: PropTypes.element,
  navItems: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      alias: PropTypes.string
    })
  ),
  actionItems: PropTypes.arrayOf(
    PropTypes.shape({
      func: PropTypes.func.isRequired,
      value: PropTypes.string
    })
  ),
  backItems: PropTypes.arrayOf(
    PropTypes.shape({
      func: PropTypes.func.isRequired,
      value: PropTypes.string
    })
  ),
  hasBackButton: PropTypes.bool,
  backIcon: PropTypes.string,
  backFunc: PropTypes.func,
  time: PropTypes.string,
  actionClasses: PropTypes.string
};
