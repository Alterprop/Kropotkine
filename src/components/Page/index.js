import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Toolbar from '../Toolbar';

export default function Page(props) {
  return (
    <ReactCSSTransitionGroup
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnterTimeout={500}
      transitionLeaveTimeout={300}
      {...props}
    />
  );
}

export function ToolbarPage(props) {
  const {
    component,
    className,
    children,
    actionItems,
    navItems,
    title,
    transitionName,
    hasBackButton,
  } = props;
  const pageProps = { component, className, transitionName };
  const toolbarProps = { actionItems, title, navItems, hasBackButton };
  return (
    <Page {...pageProps}>
      <Toolbar {...toolbarProps} />
      {children}
    </Page>
  );
}

Page.defaultProps = {
  transitionName: 'example'
};

ToolbarPage.propTypes = Page.propTypes = {
  children: PropTypes.element.isRequired,
  right: PropTypes.element,
  transitionName: PropTypes.string
};
