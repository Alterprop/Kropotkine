import React from 'react';
import PropTypes from 'prop-types';
import NavBar from '../NavBar';

const ZoocialPaths = [
  { path: '/', name: 'Linkedin', icon: 'linkedin', iconic: true },
  { path: 'https://t.me/alterprop', name: 'Telegram', icon: 'telegram', iconic: true, external: true},
  { path: 'skype:altprop?call', name: 'Skype: altprop', icon: 'skype', iconic: true, external: true }
];

export default function Zoocial(props) {

  return(
    <NavBar
      {...props}
      paths={ZoocialPaths}
      type="zoocial"
    />
  );
}

Zoocial.defaultProps = {
  role: null
};

Zoocial.propTypes = {
  role: PropTypes.bool,
};
