import React from 'react';
import PropTypes from 'prop-types';
import Icons from './icons';

export default function Icon(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.size}
      height={props.size}
      viewBox="0 0 24 24">
      <path fill={props.fill} d={Icons[props.shape]} />
    </svg>
  );
}

Icon.defaultProps = {
  fill: 'rgba(0, 0, 0, 0.56)',
  size: 24,
  type: 'regular'
};

Icon.propTypes = {
  shape: PropTypes.string.isRequired,
  fill: PropTypes.string,
  size: PropTypes.number,
  type: PropTypes.oneOf([
    'regular',
    'zoocial'
  ])
};
