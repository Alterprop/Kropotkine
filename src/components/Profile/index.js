import React from 'react';
import PropTypes from 'prop-types';
import Zoocial from '../Zoocial';
import NavBar from '../NavBar';

const MainNavPaths = [
  { path: '/web', name: 'Web', alias: 'Web' },
  { path: '/psycho', name: 'Psicogeografía', alias: 'Psycho' },
  { path: '/lit', name: 'Literatura', alias: 'Lit' }
];

export default function Profile(props) {
  console.log(props);
  return(
    <aside className="sidebar right">
      <figure>
        <img
          className={props.avatar ? 'avatar' : null }
          src={
            `https://s.gravatar.com/avatar/0dec9f7806be6c5bbbde5389742e09f1?s=${props.avatarSize}`
          }
        />
        <figcaption>
          <h1>
            Daniel Ferreira De León
          </h1>
          <small>Montevideo - Roma - Madrid - Valladolid</small>
        </figcaption>
      </figure>
      <Zoocial />
      <NavBar paths={MainNavPaths} />
    </aside>
  );
}

Profile.defaultProps = {
  role: 'navigation',
  avatar: false,
  avatarSize: 80
};

Profile.propTypes = {
  role: PropTypes.bool,
  avatar: PropTypes.bool,
  avatarSize: PropTypes.number
};
