import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListItem } from '../../components';
import { withScroll } from '../Scrollable';

class List extends Component {

  componentDidMount() {
    const { key } = this.props.books;
    if (key) {
      this.props.find(key);
    } else {
      this.props.createBook();
    }
  }

  render() {
    const { notes } = this.props.notes;
    return (
      <ul id="list" className="list" onScroll={ this.props.scrollFn }>
        {notes
          ? notes.map(note => <ListItem key={note._id} item={note} />)
          : null}
      </ul>
    );
  }
}

List.defaultProps = {
  find: null
};

List.propTypes = {
  notes: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string
    })
  ),
  books: PropTypes.shape({
    key: PropTypes.string
  }),
  find: PropTypes.func,
  createBook: PropTypes.func,
  scrollDown: PropTypes.func,
  scrollY: PropTypes.func,
};

export default withScroll(List);
