import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Page, List, Toolbar, FabButton } from '../../components';
import actions from '../../actions/actions';

function Notes(props) {
  const actionItems = [
    {
      func: props.createBook,
      name: 'nueva',
      label: 'Persistente',
      icon: 'lock',
      prompt: true
    },
    { link: '/quote', name: 'nueva', alias: 'Mascarada', icon: 'quote' }
  ];

  const fabProps = {
    icon: 'new',
    link: props.books.key ? '/notes/new' : null,
    func: !props.books.key
      ? props.createBook
      : null,
    prompt: !props.books.key,
    position: 'bottom right'
  };

  const showButton = (!props.events.scrollDown && props.location.pathname !== fabProps.link);
  const toolbarProps = {
    actionItems,
    actionClasses: `primary  ${ props.events.scrollDown || props.events.scrollY ? 'is-scrolling' : ''  } `,
    title: 'kropotkine'
  };

  return (
    <Page component="main" className="sidebar" transitionName="reveal" >
      <Toolbar {...toolbarProps} />
      {props.events.scrollDown}
      <List {...props} />
      { showButton ? <FabButton {...fabProps}  /> : null }
    </Page>
  );
}

Notes.defaultProps = {};

Notes.propTypes = {
  id: PropTypes.string,
  createBook: PropTypes.func,
  location: PropTypes.shape({
    pathname: PropTypes.string
  }),
  books: PropTypes.shape({
    key: PropTypes.string
  }),
  events: PropTypes.shape({
    scrollDown: PropTypes.bool,
    scrollY: PropTypes.bool,
  })
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)(Notes);
