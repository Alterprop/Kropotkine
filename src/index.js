import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router } from 'react-router-dom';
import App from './App';
import { Provider } from 'react-redux';
import Store from './store/store';

ReactDOM.render(
  <Provider store={Store()}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('app-root')
);
