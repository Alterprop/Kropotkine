import React from 'react';
import { arrayIncludes } from './compat';

export function formatAttributes(attributes) {
  return reactifyKeys(attributes).reduce((attrs, attribute) => {
    const { key, value } = attribute;
    if (value === null) {
      return `${attrs} ${key}`;
    }
    const quoteEscape = value.indexOf(' \' ') !== -1;
    const quote = quoteEscape ? '"' : ' \' ';
    return `${attrs} ${key}=${quote}${value}${quote}`;
  }, '');
}

function reactifyKeys(attributes) {
  const transfAttr = [ 'style', 'class' ];
  return attributes.map(attr => {
    if (transfAttr.includes(attr.key)) {
      return Object.assign({}, attr, { key: `${attr.key}Name` });
    }
    return attr;
  });

}

export function toJSX(tree, options) {
  return tree.map(node => {
    if (node.type === 'text') {
      return node.content;
    }
    if (node.type === 'comment') {
      return `<!--${node.content}-->`;
    }
    const { tagName, /*attributes,*/ children } = node;
    const isSelfClosing = arrayIncludes(
      options.voidTags,
      tagName.toLowerCase()
    );
    const Tag = tagName; // `${tagName}${formatAttributes(attributes)}`;
    return isSelfClosing ? <Tag /> : <Tag>{toJSX(children, options)}</Tag>;
  });
}

export default { toJSX };
